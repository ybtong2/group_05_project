#ifdef _WIN32
#include "stdafx.h"
#endif

#include "Group5Strat.h"


Group5Strat::Group5Strat(StrategyID strategyID,
    const std::string& strategyName,
    const std::string& groupName) :
    Strategy(strategyID, strategyName, groupName) {
}

Group5Strat::~Group5Strat() {
}

void Group5Strat::DefineStrategyParams() {
}

void Group5Strat::RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate) {
    std::cout << "Registering for the strategy event" << std::endl;
}

void Group5Strat::OnTrade(const TradeDataEventMsg& msg) {
}

void Group5Strat::OnOrderUpdate(const OrderUpdateEventMsg& msg) {
    std::cout << "name = " << msg.name() << std::endl;
    std::cout << "order id = " << msg.order_id() << std::endl;
    std::cout << "fill occurred = " << msg.fill_occurred() << std::endl;
    std::cout << "update type = " << msg.update_type() << std::endl;
    std::cout << "time " << msg.update_time() << std::endl;

}

void Group5Strat::OnBar(const BarEventMsg& msg) {
}

void Group5Strat::SendOrder(const Instrument* instrument, int trade_size) {
}

void Group5Strat::OnResetStrategyState() {
}

void Group5Strat::OnParamChanged(StrategyParam& param) {
}
